package com.lvmoney.frame.mqtt.client.config;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.client.config
 * 版本信息: 版本1.0
 * 日期:2022/3/25
 * Copyright XXXXXX科技有限公司
 *
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2022/3/25 9:43
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2022/3/25 9:43
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0
 * 2022/3/25 9:43
 */

import com.lvmoney.frame.base.core.constant.BaseConstant;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

/**
 * MQTT配置
 * @Author: songyaru
 * @Date: 2020/9/01 10:04
 * @Version 1.0
 */

@Configuration
@IntegrationComponentScan
public class MqttReceiveConfig {

    @Value("${spring.mqtt.username}")
    private String username;

    @Value("${spring.mqtt.password}")
    private String password;

    @Value("${spring.mqtt.url}")
    private String hostUrl;

    @Value("${spring.mqtt.client.id}")
    private String clientId;

    @Value("${spring.mqtt.default.topic}")
    private String defaultTopic;
    /**
     * 连接超时
     */
    @Value("${spring.mqtt.default.completionTimeout}")
    private int completionTimeout;

    /**
     * 连接超时
     */
    @Value("${spring.mqtt.default.keepAliveInterval:50}")
    private int keepAliveInterval;

    /**
     * 连接超时
     */
    @Value("${spring.mqtt.default.qos:1}")
    private int qos;


    /**
     * 初始化连接
     *
     * @throws
     * @return: org.eclipse.paho.client.mqttv3.MqttConnectOptions
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2022/3/25 9:48
     */

    @Bean
    public MqttConnectOptions getMqttConnectOptions() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        mqttConnectOptions.setServerURIs(new String[]{hostUrl});
        mqttConnectOptions.setKeepAliveInterval(keepAliveInterval);
        return mqttConnectOptions;
    }

    /**
     *初始化mqtt工厂
     * @return: org.springframework.integration.mqtt.core.MqttPahoClientFactory
     * @throws
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2022/3/25 9:48
     */

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setConnectionOptions(getMqttConnectOptions());
        return factory;
    }

    /**
     *接收通道
     * @return: org.springframework.messaging.MessageChannel
     * @throws
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2022/3/25 9:48
     */

    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    /**
     * 配置client,监听的topic
     * @param messageChannel:
     * @return: org.springframework.integration.core.MessageProducer
     * @throws
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2022/3/25 9:48
     */

    @Bean
    public MessageProducer inbound(@Qualifier("mqttInputChannel") MessageChannel messageChannel) {
        //这里的defaultTopic是发布者的主题
        MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter(clientId, mqttClientFactory(), getTopics());
        adapter.setCompletionTimeout(completionTimeout);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(qos);
        adapter.setOutputChannel(messageChannel);
        return adapter;
    }

    /**
     *订阅消费数据,通过通道获取数据
     * @return: org.springframework.messaging.MessageHandler
     * @throws
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2022/3/25 9:49
     */
    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
        return message -> {
            System.out.println("主题：{}，消息接收到的数据：{}");
            System.out.println(message.getHeaders().get("mqtt_receivedTopic"));
            System.out.println(message.getPayload());
        };
    }

    /**
     * 获得topic 数组
     *
     * @throws
     * @return: org.springframework.messaging.MessageHandler
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2022/3/25 9:49
     */
    private String[] getTopics() {
        if (defaultTopic.contains(BaseConstant.CHAR_COMMA)) {
            return defaultTopic.split(BaseConstant.CHAR_COMMA);
        } else {
            return new String[]{defaultTopic};
        }

    }

}

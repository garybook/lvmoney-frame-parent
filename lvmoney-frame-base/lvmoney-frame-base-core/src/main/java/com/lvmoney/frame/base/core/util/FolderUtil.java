package com.lvmoney.frame.base.core.util;/**
 * 描述:
 * 包名:com.lvmoney.frame.expression.util
 * 版本信息: 版本1.0
 * 日期:2019/11/19
 * Copyright 四川******科技有限公司
 */

import java.io.File;
import java.util.Optional;

/**
 * @describe：
 * @author: lvmoney /四川******科技有限公司
 * @version:v1.0 2019/11/19 16:38
 */
public class FolderUtil {


    /**
     * 递归替换文件和目录名
     *
     * @param file         根目录地址
     * @param findValue    需要替换的值
     * @param replaceValue 替换值
     * @return
     * @throws
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2022/2/14 17:25
     */

    public static void changeName(File file, String findValue, String replaceValue) {
        for (File subdirectory : Optional.ofNullable(file.listFiles()).orElse(new File[]{})) {
            if (subdirectory.isDirectory() && subdirectory.listFiles().length > 0) {
                changeName(subdirectory, findValue, replaceValue);
            } else {
                String newName = subdirectory.getParent() + File.separator + subdirectory.getName().replace(findValue, replaceValue);
                subdirectory.renameTo(new File(newName));
            }
        }
        String newName = file.getParent() + File.separator + file.getName().replace(findValue, replaceValue);
        file.renameTo(new File(newName));
        System.out.println("操作成功");
    }


    public static void main(String[] args) {
        // 根目录地址
        String path = "D:\\work\\code\\stkj-frame-parent";
        // 需要替换的值
        String findValue = "boao";
        // 替换值
        String replaceValue = "stkj";
        // 调用递归替换文件和目录名方法
        changeName(new File(path), findValue, replaceValue);
    }

}


